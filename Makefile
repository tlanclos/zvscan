CC=gcc

CFLAGS+=-Wall
CFLAGS+=-fPIC
CFLAGS+=-std=c99
CFLAGS+=-MMD
CFALGS+=-MP

LDFLAGS+=-lopencv
LDFLAGS+=-lzbar
LDFLAGS+=-lm
LDFLAGS+=-D_BSD_SOURCE

SRC_DIR=src
BLD_DIR=build

OBJS=zvs_scanner.o zvs_camera.o
OBJECTS=$(patsubst %, $(BLD_DIR)/%, $(OBJS))

HEAD=$(OBJS:.o=.h)
HEADERS=$(patsubst %, $(SRC_DIR)/%, $(HEAD))

HEADNC=zvs_types.h zvscan.h
HEADERSNC=$(patsubst %, $(SRC_DIR)/%, $(HEADNC))

OUT_A=libzvscan.a
OUT_SO=libzvscan.so

all: clean $(BLD_DIR) $(OBJECTS) $(OUT_A) $(OUT_SO)

$(BLD_DIR):
	mkdir -p $(BLD_DIR)

$(BLD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -c $(LDFLAGS) -o $@ $< $(CFLAGS)

$(OUT_SO): $(OBJECTS)
	$(CC) -shared -o $@ $^
	
$(OUT_A): $(OBJECTS)
	ar rcs $@ $^

install: $(OUT)
	mkdir -p /usr/include/zvscan
	cp $(OUT_A) /usr/lib/.
	cp $(HEADERS) /usr/include/zvscan/.
	cp $(HEADERSNC) /usr/include/zvscan/.
	cp zvscan.pc /usr/lib/pkgconfig/.

uninstall:
	rm /usr/lib/$(OUT_A)
	rm -rf /usr/include/zvscan
	rm /usr/lib/pkgconfig/zvscan.pc

clean:
	rm -rf $(BLD_DIR) $(OUT_A) $(OUT_SO) 
