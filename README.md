<TODO: ADD DOCUMENTATION ON FUNCTIONS>

-------------------------------------------------------------
SAMPLE CODE:
-------------------------------------------------------------

#include <zvscan/zvscan.h>

static camera_t cam;
static scanner_t sn;

static int fv[31] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
                     15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};

int main(void) {
    char * data = NULL;
    int i;
    
    cam.width = 640;
    cam.height = 480;
    cam.pixel_format = V4L2_PIX_FMT_MJPEG;
    
    init_device(&cam, "/dev/video0", ZVS_WRITE_IMAGES);
    set_camera_output_file(&cam, "camera.jpg");
    
    init_scanner(&sn, ZVS_WRITE_IMAGES | ZVS_SHOW_IMAGES);
    set_scanner_output_file(&sn, "scanner.jpg");
    
    while (true) {
        i = 0;
        for (i; i < 31; i++) {
            set_focus(&cam, fv[i]);
            capture(&cam);
            data = scan_bar(&sn, &cam);
        }
	if (data == NULL)
	    break;
    }
    
    printf("%s", data);
    
    return 0;
}
-------------------------------------------------------------
