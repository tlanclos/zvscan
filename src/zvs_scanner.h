/* 
 * File:   scanner.h
 * Author: Taylor Lanclos
 *
 * Created on February 9, 2014, 12:31 PM
 */

#ifndef ZVS_SCANNER_H
#define	ZVS_SCANNER_H

#include <zbar.h>

#include "zvs_types.h"
#include "zvs_camera.h"

extern void init_scanner(scanner_t * scanner, int options);

extern void set_scanner_output_file(scanner_t * scanner, const char * filepath);

extern char * scan_bar(scanner_t * scanner, camera_t * camera);

extern void clean_scanner(scanner_t * scanner);

#endif	/* ZVS_SCANNER_H */

