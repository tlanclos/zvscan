#include "zvs_scanner.h"

void init_scanner(scanner_t * scanner, int options) {
    scanner->scanner = zbar_image_scanner_create();
    zbar_image_scanner_set_config(scanner->scanner, ZBAR_NONE, 
                                  ZBAR_CFG_ENABLE, 1);
    scanner->options = options;
}

void set_scanner_output_file(scanner_t * scanner, const char * filepath) {
    scanner->filepath = (char *)filepath;
}

char * scan_bar(scanner_t * scanner, camera_t * camera) {
    IplImage * t = threshold(camera);
    const zbar_symbol_t * symbol;
    char * data;
    int n = 0;
    
    if (scanner->options & ZVS_WRITE_IMAGES) {
        write_file(t, scanner->filepath);
    }
    
    if (scanner->options & ZVS_SHOW_IMAGES) {
        cvShowImage("threshold", t);
        cvWaitKey(1);
    }
    
    scanner->image = zbar_image_create();
    zbar_image_set_format(scanner->image, *(int*)"Y800");
    zbar_image_set_size(scanner->image, camera->width, camera->height);
    zbar_image_set_data(scanner->image, t->imageData, t->imageSize, zbar_image_free_data);

    n = zbar_scan_image(scanner->scanner, scanner->image);
    
    cvReleaseImage(&t);
    
    switch (n) {
        case 0:
            data = NULL;
            break;
        default:
            symbol = zbar_image_first_symbol(scanner->image);
            data = (char *)zbar_symbol_get_data(symbol);
            break;
    };
    
    return data;
}

void clean_scanner(scanner_t * scanner) {
    zbar_image_destroy(scanner->image);
    zbar_image_scanner_destroy(scanner->scanner);
}
