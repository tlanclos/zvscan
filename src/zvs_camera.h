/* 
 * File:   camera.h
 * Author: Taylor Lanclos
 *
 * Created on February 9, 2014, 11:25 AM
 */

#ifndef ZVS_CAMERA_H
#define	ZVS_CAMERA_H

#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <linux/videodev2.h>

#include "zvs_types.h"

#define IMG_CAPTURE_TIMEOUT     5
#define IMG_FOCUS_WAIT_US       800

extern void perr(const char* message, bool fail);

extern int xioctl(int fd, int request, void *arg);

extern void write_file(IplImage * image, const char * filepath);

extern void init_device(camera_t * camera, const char * device, int options);

extern void set_camera_output_file(camera_t * camera, const char * filepath);

extern void set_focus(camera_t * camera, int focus);

extern void capture(camera_t * camera);

extern IplImage * threshold(camera_t * camera);

#endif	/* ZVS_CAMERA_H */

