#include "zvs_camera.h"

void perr(const char* message, bool fail) {
    perror(message);
    //if (fail)
      //  nice_quit();
}

int xioctl(int fd, int request, void *arg) {
    int r;
    do r = ioctl(fd, request, arg);
    while (-1 == r && EINTR == errno);
    return r;
}

void write_file(IplImage * image, const char * filepath) {
    cvSaveImage(filepath, image, NULL);
}

void init_device(camera_t * camera, const char * device, int options) {
    camera->options = options;
    camera->fd = open(device, O_RDWR);
    
    struct v4l2_capability caps = {{0}};
    if (-1 == xioctl(camera->fd, VIDIOC_QUERYCAP, &caps))
        perr("device not compatible with v4l2", true);
    
    struct v4l2_format fmt = {0};
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width = camera->width;
    fmt.fmt.pix.height = camera->height;
    fmt.fmt.pix.pixelformat = camera->pixel_format;
    fmt.fmt.pix.field = V4L2_FIELD_NONE;
    if (-1 == xioctl(camera->fd, VIDIOC_S_FMT, &fmt))
        perr("width x height or format not supported by device", true);
    
    struct v4l2_requestbuffers req = {0};
    req.count = 1;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;
    if (-1 == xioctl(camera->fd, VIDIOC_REQBUFS, &req))
        perr("failed to request buffer from device", false);
    
    camera->buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    camera->buffer.memory = V4L2_MEMORY_MMAP;
    camera->buffer.index = 0;
    
    if (-1 == xioctl(camera->fd, VIDIOC_QBUF, &camera->buffer))
        perr("failed to queue buffer", false);
    
    
    if (-1 == xioctl(camera->fd, VIDIOC_QUERYBUF, &camera->buffer))
        perr("buffer failed to init", false);
    
    
    camera->_data = mmap(NULL, camera->buffer.length, PROT_READ | PROT_WRITE, 
                         MAP_SHARED, camera->fd, camera->buffer.m.offset);
            
    if (-1 == xioctl(camera->fd, VIDIOC_STREAMON, &camera->buffer.type))
        perr("failed to initialize video stream", false);
    
    if (camera->options & ZVS_AUTO_FOCUS) {
        struct v4l2_queryctrl qf_auto = {0};
        qf_auto.id = V4L2_CID_FOCUS_AUTO;
        
        struct v4l2_control f_auto = {0};
        f_auto.id = V4L2_CID_FOCUS_AUTO;
        f_auto.value = 1;
    
        if (-1 != xioctl(camera->fd, VIDIOC_QUERYCTRL, &qf_auto)) {
            if (-1 == xioctl(camera->fd, VIDIOC_S_CTRL, &f_auto))
                perr("unable to set auto focus", false);
        }
    } else if (camera->options & ZVS_ABSOLUTE_FOCUS) {
        struct v4l2_queryctrl qf_auto = {0};
        qf_auto.id = V4L2_CID_FOCUS_AUTO;
        
        struct v4l2_control f_auto = {0};
        f_auto.id = V4L2_CID_FOCUS_AUTO;
        f_auto.value = 0;
    
        if (-1 != xioctl(camera->fd, VIDIOC_QUERYCTRL, &qf_auto)) {
            if (-1 == xioctl(camera->fd, VIDIOC_S_CTRL, &f_auto))
                perr("unable to set auto focus", false);
        }
    }
}

void set_camera_output_file(camera_t * camera, const char * filepath) {
    camera->filepath = (char *)filepath;
}

void set_focus(camera_t * camera, int focus) {
    struct v4l2_queryctrl qf_abs = {0};
    qf_abs.id = V4L2_CID_FOCUS_ABSOLUTE;
    
    if (-1 != xioctl(camera->fd, VIDIOC_QUERYCTRL, &qf_abs)) {
        
        struct v4l2_control f_abs = {0};
        f_abs.id = V4L2_CID_FOCUS_ABSOLUTE;
        f_abs.value = focus;

        if (-1 == xioctl(camera->fd, VIDIOC_S_CTRL, &f_abs))
            perr("unable to set absolute focus", false);
    }
    usleep(IMG_FOCUS_WAIT_US);
}

void capture(camera_t * camera) {    
    struct timeval timeout = {0};
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(camera->fd, &fds);
    
    timeout.tv_sec = IMG_CAPTURE_TIMEOUT;
    
    int r = select(camera->fd + 1, &fds, NULL, NULL, &timeout);
    if (-1 == r)
        perr("file descriptor not readying up", false);
    
    if (-1 == xioctl(camera->fd, VIDIOC_DQBUF, &camera->buffer))
        perr("failed to dequeue buffer", false);
    
    CvMat cvmat = cvMat(camera->height, camera->width, CV_8UC1, camera->_data);
    IplImage * temp = cvDecodeImage(&cvmat, 1);
    
    if (camera->options & ZVS_WRITE_IMAGES) {
        write_file(temp, camera->filepath);
    }
    
    if (camera->options & ZVS_SHOW_IMAGES) {
        cvShowImage("camera", temp);
        cvWaitKey(1);
    }
    
    camera->captured_data = cvCreateImage(cvGetSize(temp), IPL_DEPTH_8U, 1);
    cvCvtColor(temp, camera->captured_data, CV_RGB2GRAY);
    cvReleaseImage(&temp);
    
    if (-1 == xioctl(camera->fd, VIDIOC_QBUF, &camera->buffer))
        perr("failed to queue buffer", false);
    
    if (-1 == xioctl(camera->fd, VIDIOC_QUERYBUF, &camera->buffer))
        perr("buffer failed to init", false);
}

IplImage * threshold(camera_t * camera) {
    IplImage * threshimg = cvCreateImage(cvGetSize(camera->captured_data), 
                                         IPL_DEPTH_8U, 1);
    cvThreshold(camera->captured_data, threshimg, 127, 255, 
                CV_THRESH_BINARY | CV_THRESH_OTSU);
    
    return threshimg;
}
