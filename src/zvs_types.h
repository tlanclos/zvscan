/* 
 * File:   zvs_types.h
 * Author: Taylor Lanclos
 *
 * Created on February 9, 2014, 9:04 PM
 */

#ifndef ZVS_TYPES_H
#define ZVS_TYPES_H

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <linux/videodev2.h>

#include <zbar.h>

enum {
    ZVS_NONE            = 0x0,
    ZVS_SHOW_IMAGES     = 0x1,
    ZVS_WRITE_IMAGES    = 0x2,
    ZVS_AUTO_FOCUS      = 0x4,
    ZVS_ABSOLUTE_FOCUS  = 0x8
};

typedef struct _camera {
    int fd;
    int width;
    int height;
    int pixel_format;
    struct v4l2_buffer buffer;
    IplImage * captured_data;
    void * _data;
    char * filepath;
    int options;
} camera_t;

typedef struct _scanner {
    zbar_image_scanner_t * scanner;
    zbar_image_t * image;
    char * filepath;
    int options;
} scanner_t;

#endif	/* ZVS_TYPES_H */
